/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strmap.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gdaniel <gdaniel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/23 14:48:55 by gdaniel           #+#    #+#             */
/*   Updated: 2018/11/27 17:16:51 by gdaniel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strmap(char const *s, char (*f)(char))
{
	size_t	len;
	size_t	count;
	char	*str;

	if (s == NULL)
		return (NULL);
	len = ft_strlen(s);
	count = 0;
	str = ft_strnew(len);
	if (str == NULL)
		return (NULL);
	while (*s)
	{
		*str++ = f(*s++);
		count++;
	}
	str -= count;
	return (str);
}
