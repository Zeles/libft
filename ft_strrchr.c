/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strrchr.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gdaniel <gdaniel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/22 16:21:42 by gdaniel           #+#    #+#             */
/*   Updated: 2018/12/03 16:24:02 by gdaniel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strrchr(const char *s, int c)
{
	size_t				len;
	const unsigned char	*tmpstr;

	len = ft_strlen(s);
	tmpstr = (const unsigned char*)s;
	if (!c)
		tmpstr += len;
	else
		tmpstr += len - 1;
	if (c == '\0')
		len = 1;
	while ((*tmpstr || *tmpstr == (unsigned char)c) && len)
	{
		if (*tmpstr == (unsigned char)c)
			return ((char*)tmpstr);
		tmpstr--;
		len--;
	}
	return (NULL);
}
