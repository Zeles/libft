/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_itoa.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gdaniel <gdaniel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/23 13:18:58 by gdaniel           #+#    #+#             */
/*   Updated: 2018/12/03 14:45:57 by gdaniel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static size_t	ft_intlen(int n)
{
	size_t len;

	len = 0;
	if (n)
		while (n != 0)
		{
			n /= 10;
			len++;
		}
	else
		len++;
	return (len);
}

static int		ft_isnegative(int *n, size_t *len)
{
	if (*n < 0)
	{
		*n *= -1;
		*len += 1;
		return (1);
	}
	return (0);
}

static char		*ft_returnminint(char *result)
{
	result = ft_strsub("-2147483648", 0, 12);
	return (result);
}

char			*ft_itoa(int n)
{
	char	*result;
	int		flag;
	size_t	len;

	len = 0;
	result = NULL;
	if (n == -2147483648)
		return (ft_returnminint(result));
	flag = ft_isnegative(&n, &len);
	len += ft_intlen(n);
	result = ft_strnew(len);
	if (result == NULL)
		return (NULL);
	if (flag)
		result[0] = '-';
	len--;
	if (n == 0)
		result[flag] = '0';
	while (n != 0)
	{
		result[len--] = '0' + n % 10;
		n /= 10;
	}
	return (result);
}
