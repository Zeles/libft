/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strnequ.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gdaniel <gdaniel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/23 15:04:02 by gdaniel           #+#    #+#             */
/*   Updated: 2018/11/27 16:32:47 by gdaniel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int		ft_strnequ(char const *s1, char const *s2, size_t n)
{
	int i;

	if ((s1 != NULL) && (s2 != NULL))
	{
		i = ft_strncmp(s1, s2, n);
		if (i == 0)
			return (1);
	}
	return (0);
}
