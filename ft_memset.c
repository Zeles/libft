/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memset.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: wqarro-v <wqarro-v@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/20 14:44:41 by gdaniel           #+#    #+#             */
/*   Updated: 2018/12/12 18:52:52 by wqarro-v         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memset(void *dst, int c, size_t n)
{
	unsigned char	*tmp;
	char			tmpchar;

	tmp = dst;
	tmpchar = c;
	while (n)
	{
		*tmp++ = tmpchar;
		n--;
	}
	return ((void*)dst);
}
