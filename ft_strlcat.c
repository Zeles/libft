/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strlcat.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gdaniel <gdaniel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/22 14:23:06 by gdaniel           #+#    #+#             */
/*   Updated: 2018/11/28 16:16:55 by gdaniel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

size_t	ft_strlcat(char *dst, const char *src, size_t size)
{
	size_t	count;
	size_t	lensrc;
	size_t	lendst;

	count = 0;
	lendst = 0;
	lensrc = 0;
	while (src[lensrc])
		lensrc++;
	while (dst[count])
		count++;
	lendst = count;
	if (size)
	{
		dst += count;
		while (count < size - 1 && *src)
		{
			*dst++ = *src++;
			count++;
		}
		*dst = '\0';
	}
	return (lendst < size ? lendst + lensrc : size + lensrc);
}
