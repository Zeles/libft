/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strtrim.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gdaniel <gdaniel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/23 16:00:46 by gdaniel           #+#    #+#             */
/*   Updated: 2018/11/30 13:18:49 by gdaniel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strtrim(char const *s)
{
	size_t	len;
	char	*result;

	if (s == NULL)
		return (NULL);
	while (*s == ' ' || *s == '\n' || *s == '\t')
		s++;
	len = ft_strlen(s);
	if (len)
	{
		s += len - 1;
		while (*s == ' ' || *s == '\n' || *s == '\t')
		{
			len--;
			s--;
		}
		s -= len - 1;
		result = ft_strsub(s, 0, len);
		if (result == NULL)
			return (NULL);
		result[len] = '\0';
	}
	else
		result = ft_strnew(1);
	return (result);
}
