/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memchr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gdaniel <gdaniel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/21 14:10:07 by gdaniel           #+#    #+#             */
/*   Updated: 2018/12/04 12:48:44 by gdaniel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memchr(const void *s, int c, size_t n)
{
	unsigned char		cfind;
	const unsigned char	*tmp;

	cfind = (unsigned char)c;
	tmp = (const unsigned char*)s;
	while (n)
	{
		if (*tmp == cfind)
			return ((void*)tmp);
		tmp++;
		n--;
	}
	return (NULL);
}
