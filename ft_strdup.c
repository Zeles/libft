/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strdup.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gdaniel <gdaniel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/19 13:35:42 by gdaniel           #+#    #+#             */
/*   Updated: 2018/11/26 14:39:31 by gdaniel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "libft.h"

char	*ft_strdup(const char *src)
{
	size_t		len;
	char		*result;

	len = ft_strlen(src);
	result = malloc(sizeof(char) * (len + 1));
	if (result == NULL)
		return (NULL);
	len = 0;
	while (src[len])
	{
		result[len] = src[len];
		len++;
	}
	result[len] = '\0';
	return (result);
}
