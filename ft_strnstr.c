/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strnstr.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gdaniel <gdaniel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/23 11:21:22 by gdaniel           #+#    #+#             */
/*   Updated: 2018/12/04 13:03:32 by gdaniel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strnstr(const char *str, const char *to_find, size_t len)
{
	size_t count;
	size_t count2;

	count = 0;
	if (*to_find == '\0')
		return ((char*)str);
	while ((str[count] || str[count] == to_find[0]) && count < len)
	{
		if (str[count] == to_find[0])
		{
			count2 = 1;
			while (str[count + count2] == to_find[count2]
			&& to_find[count2] != '\0' && (count + count2) < len)
				count2++;
			if (to_find[count2] == '\0')
				return ((char*)&str[count]);
		}
		count++;
	}
	return (NULL);
}
