/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strsub.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gdaniel <gdaniel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/23 15:24:00 by gdaniel           #+#    #+#             */
/*   Updated: 2018/11/27 16:35:42 by gdaniel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strsub(char const *s, unsigned int start, size_t len)
{
	size_t	count;
	char	*result;

	if (s == NULL)
		return (NULL);
	count = 0;
	result = ft_strnew(len);
	if (result == NULL)
		return (NULL);
	s += start;
	while (count < len)
	{
		result[count] = s[count];
		count++;
	}
	return (result);
}
