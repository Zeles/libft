/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memmove.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gdaniel <gdaniel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/22 11:21:01 by gdaniel           #+#    #+#             */
/*   Updated: 2018/12/03 16:59:32 by gdaniel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memmove(void *dst, const void *src, size_t n)
{
	unsigned char		*tmp;
	const unsigned char	*srctmp;

	tmp = dst;
	srctmp = src;
	if (tmp < srctmp)
		while (n)
		{
			*tmp++ = *srctmp++;
			n--;
		}
	else
	{
		tmp += n - 1;
		srctmp += n - 1;
		while (n)
		{
			*tmp-- = *srctmp--;
			n--;
		}
		tmp++;
	}
	return (dst);
}
