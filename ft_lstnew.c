/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstnew.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gdaniel <gdaniel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/30 20:54:42 by gdaniel           #+#    #+#             */
/*   Updated: 2018/12/03 17:24:04 by gdaniel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <stdlib.h>

t_list	*ft_lstnew(void const *content, size_t content_size)
{
	t_list *result;

	result = (t_list*)malloc(1 * sizeof(t_list));
	if (result != NULL)
	{
		if (content != NULL && content_size != 0)
		{
			result->content = (void*)malloc(content_size);
			ft_memcpy(result->content, content, content_size);
			result->content_size = content_size;
		}
		else
		{
			result->content = NULL;
			result->content_size = 0;
		}
		result->next = NULL;
		return (result);
	}
	return (NULL);
}
