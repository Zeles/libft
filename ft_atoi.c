/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_atoi.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gdaniel <gdaniel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/27 11:37:29 by gdaniel           #+#    #+#             */
/*   Updated: 2018/12/03 10:25:28 by gdaniel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int		ft_atoi(const char *str)
{
	int result;
	int multipl;

	result = 0;
	multipl = 1;
	while (*str == ' ' || *str == '\t' || *str == '\f' || *str == '\v'
		|| *str == '\r' || *str == '\n')
		str++;
	if ((*str == '-' || *str == '+') &&
	(*(str + 1) == '-' || *(str + 1) == '+'))
		return (0);
	if (*str == '-')
	{
		multipl = -1;
		str++;
	}
	if (*str == '+')
		str++;
	while (ft_isdigit(*str))
	{
		result *= 10;
		result += *str - 48;
		str++;
	}
	return (result * multipl);
}
