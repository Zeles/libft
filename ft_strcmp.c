/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strcmp.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gdaniel <gdaniel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/21 13:29:31 by gdaniel           #+#    #+#             */
/*   Updated: 2018/11/26 15:31:42 by gdaniel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int		ft_strcmp(const char *str1, const char *str2)
{
	size_t				i;
	const unsigned char	*tmpstr1;
	const unsigned char	*tmpstr2;

	i = 0;
	tmpstr1 = (unsigned char*)str1;
	tmpstr2 = (unsigned char*)str2;
	while ((tmpstr1[i] == tmpstr2[i]) &&
	(tmpstr1[i] != '\0' && tmpstr2[i] != '\0'))
		i++;
	return (tmpstr1[i] - tmpstr2[i]);
}
