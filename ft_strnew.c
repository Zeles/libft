/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strnew.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: wqarro-v <wqarro-v@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/22 10:45:27 by gdaniel           #+#    #+#             */
/*   Updated: 2018/12/12 18:53:11 by wqarro-v         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "libft.h"

char	*ft_strnew(size_t size)
{
	char	*result;

	result = (char*)malloc((size + 1) * sizeof(char));
	if (result == NULL)
		return (NULL);
	ft_memset(result, 0, size + 1);
	return (result);
}
