/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memccpy.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gdaniel <gdaniel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/21 11:39:45 by gdaniel           #+#    #+#             */
/*   Updated: 2018/12/04 12:22:42 by gdaniel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memccpy(void *dst, const void *src, int c, size_t n)
{
	unsigned char		cfind;
	unsigned char		*tmp;
	const unsigned char	*srctmp;
	size_t				count;

	cfind = c;
	tmp = (unsigned char*)dst;
	srctmp = (const unsigned char*)src;
	count = 0;
	while (count < n)
	{
		if (*srctmp == cfind)
		{
			*tmp++ = *srctmp++;
			count++;
			dst += count;
			return (dst);
		}
		*tmp++ = *srctmp++;
		count++;
	}
	return (NULL);
}
