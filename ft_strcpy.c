/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strcpy.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gdaniel <gdaniel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/27 16:28:56 by gdaniel           #+#    #+#             */
/*   Updated: 2018/12/03 16:37:09 by gdaniel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strcpy(char *dst, const char *src)
{
	size_t	count;

	count = 0;
	while (src[count])
	{
		dst[count] = (unsigned char)src[count];
		count++;
	}
	dst[count] = '\0';
	return (dst);
}
