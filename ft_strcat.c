/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strcat.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gdaniel <gdaniel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/20 15:40:41 by gdaniel           #+#    #+#             */
/*   Updated: 2018/11/22 13:57:03 by gdaniel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strcat(char *dst, const char *src)
{
	size_t	lensrc;
	size_t	lendstptr;
	size_t	len;
	size_t	count;

	lensrc = ft_strlen(src);
	lendstptr = ft_strlen(dst);
	len = lendstptr + lensrc;
	count = 0;
	while (lendstptr <= len)
	{
		dst[lendstptr] = src[count];
		lendstptr++;
		count++;
	}
	return (dst);
}
