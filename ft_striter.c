/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_striter.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gdaniel <gdaniel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/22 10:52:11 by gdaniel           #+#    #+#             */
/*   Updated: 2018/12/04 13:36:18 by gdaniel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	ft_striter(char *s, void (*f)(char *))
{
	size_t len;
	size_t count;

	if (s != NULL && f != NULL)
	{
		len = ft_strlen(s);
		count = 0;
		while (count < len)
		{
			f(&s[count]);
			count++;
		}
	}
}
