/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_striteri.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gdaniel <gdaniel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/22 10:55:58 by gdaniel           #+#    #+#             */
/*   Updated: 2018/12/04 13:36:27 by gdaniel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	ft_striteri(char *s, void (*f)(unsigned int, char *))
{
	size_t len;
	size_t count;

	if (s != NULL && f != NULL)
	{
		len = ft_strlen(s);
		count = 0;
		while (count < len)
		{
			f(count, &s[count]);
			count++;
		}
	}
}
