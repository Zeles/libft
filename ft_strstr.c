/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strstr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gdaniel <gdaniel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/28 19:27:40 by gdaniel           #+#    #+#             */
/*   Updated: 2018/11/28 12:17:46 by gdaniel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strstr(const char *str, const char *to_find)
{
	size_t count;
	size_t count2;

	count = 0;
	if (*to_find == '\0')
		return ((char*)str);
	while (str[count] != '\0' || str[count] == to_find[0])
	{
		if (str[count] == to_find[0])
		{
			count2 = 1;
			while (str[count + count2] == to_find[count2]
			&& to_find[count2] != '\0')
				count2++;
			if (to_find[count2] == '\0')
				return ((char*)&str[count]);
		}
		count++;
	}
	return (NULL);
}
