/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strmapi.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gdaniel <gdaniel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/23 14:50:56 by gdaniel           #+#    #+#             */
/*   Updated: 2018/11/27 19:11:02 by gdaniel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strmapi(char const *s, char (*f)(unsigned int, char))
{
	unsigned int	count;
	size_t			len;
	char			*str;

	if (s == NULL)
		return (NULL);
	len = ft_strlen(s);
	count = 0;
	str = ft_strnew(len * sizeof(char));
	if (str == NULL)
		return (NULL);
	while (*s)
	{
		*str++ = f(count, *s++);
		count++;
	}
	str -= count;
	return (str);
}
