/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strncat.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gdaniel <gdaniel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/20 17:22:48 by gdaniel           #+#    #+#             */
/*   Updated: 2018/11/28 13:05:59 by gdaniel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strncat(char *dst, const char *src, size_t n)
{
	char *tmp;

	tmp = dst;
	while (*tmp)
		tmp++;
	while (n && *src)
	{
		*tmp++ = *src++;
		n--;
	}
	if (*src == '\0')
		*tmp++ = *src++;
	if (!n && src != '\0')
		*tmp = '\0';
	return (dst);
}
