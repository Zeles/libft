/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memcmp.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gdaniel <gdaniel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/21 14:13:58 by gdaniel           #+#    #+#             */
/*   Updated: 2018/11/21 14:39:48 by gdaniel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int		ft_memcmp(const void *str1, const void *str2, size_t n)
{
	size_t				count;
	const unsigned char	*tmp1;
	const unsigned char	*tmp2;

	count = 0;
	tmp1 = str1;
	tmp2 = str2;
	while (n)
	{
		if ((tmp1[count] != tmp2[count]))
			return (tmp1[count] - tmp2[count]);
		count++;
		n--;
	}
	return (0);
}
