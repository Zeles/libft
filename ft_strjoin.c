/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strjoin.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: wqarro-v <wqarro-v@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/23 15:32:18 by gdaniel           #+#    #+#             */
/*   Updated: 2018/12/12 18:45:03 by wqarro-v         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <stdlib.h>

char	*ft_strjoin(char const *s1, char const *s2)
{
	size_t	len;
	char	*result;

	if (s1 == NULL || s2 == NULL)
		return (NULL);
	len = ft_strlen(s1) + ft_strlen(s2);
	if (!(result = (char*)malloc((len + 1) * sizeof(char))))
		return (NULL);
	while (*s1 || *s2)
	{
		if (*s1)
			*result++ = *s1++;
		else if (*s2)
			*result++ = *s2++;
	}
	*result = '\0';
	result = result - len;
	return (result);
}
