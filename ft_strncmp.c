/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strncmp.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gdaniel <gdaniel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/22 17:20:52 by gdaniel           #+#    #+#             */
/*   Updated: 2018/12/03 15:26:44 by gdaniel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int		ft_strncmp(const char *str1, const char *str2, size_t num)
{
	size_t				i;
	const unsigned char	*tmpstr1;
	const unsigned char	*tmpstr2;

	i = 0;
	tmpstr1 = (unsigned char*)str1;
	tmpstr2 = (unsigned char*)str2;
	while ((tmpstr1[i] == tmpstr2[i])
		&& (tmpstr1[i] != '\0' && tmpstr2[i] != '\0') && num)
	{
		num--;
		i++;
	}
	return (!num ? 0 : tmpstr1[i] - tmpstr2[i]);
}
