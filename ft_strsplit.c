/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strsplit.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gdaniel <gdaniel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/23 16:00:05 by gdaniel           #+#    #+#             */
/*   Updated: 2018/12/03 13:32:16 by gdaniel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <stdlib.h>

static size_t	word_len(char *str, char c)
{
	size_t len;

	len = 0;
	while (str[len] != c && str[len] != '\0')
		len++;
	return (len);
}

static size_t	words_count(const char *str, char c, size_t len)
{
	int		flag;
	size_t	count;
	size_t	word;
	size_t	wordlen;

	flag = 0;
	count = 0;
	word = 0;
	while (count < len)
	{
		while (*str == c && *str != '\0')
		{
			str++;
			count++;
		}
		if ((wordlen = word_len((char*)str, c)) > 0)
		{
			word++;
			str += wordlen;
			count += wordlen;
		}
	}
	return (word);
}

char			**ft_strsplit(char const *s, char c)
{
	size_t	count;
	char	**result;

	if (s == NULL)
		return (NULL);
	count = 0;
	while (*s == c)
		s++;
	result = (char**)ft_memalloc((words_count(s, c,
	ft_strlen(s)) + 1) * sizeof(char*));
	if (result == NULL)
		return (NULL);
	while (*s != '\0')
	{
		result[count] = ft_strsub(s, 0, word_len((char*)s, c));
		if (result[count] == NULL)
			return (NULL);
		s += word_len((char*)s, c);
		while (*s == c)
			s++;
		if (*s == '\0')
			return (result);
		count++;
	}
	return (result);
}
