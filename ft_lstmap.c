/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstmap.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gdaniel <gdaniel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/03 18:46:35 by gdaniel           #+#    #+#             */
/*   Updated: 2018/12/04 12:11:07 by gdaniel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

t_list	*ft_lstmap(t_list *lst, t_list *(*f)(t_list *elem))
{
	t_list *tmp;
	t_list *result;
	t_list *result_start;

	result_start = NULL;
	if (lst != NULL)
	{
		tmp = lst;
		result = f(tmp);
		result = ft_lstnew(result->content, result->content_size);
		tmp = tmp->next;
		result_start = result;
		while (tmp != NULL)
		{
			result->next = f(tmp);
			result->next = ft_lstnew(result->next->content,
			result->next->content_size);
			result = result->next;
			if (tmp->next != NULL)
				tmp = tmp->next;
			else
				tmp = NULL;
		}
	}
	return (result_start);
}
